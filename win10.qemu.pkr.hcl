variable "cpu_cores" {
  type = number
  default = 4
}

variable "memory" {
  type = number
  default = 4096
}

variable "headless" {
  type = bool
  default = false
}

source "qemu" "bootstrap" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  boot_command             = ["<enter>"]
  boot_wait                = "1s"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  cd_files                 = ["drivers/*", "Autounattend.xml", "configure.ps1", "drivers.ps1", "winrm.ps1", "stage1.ps1", "stage2.ps1"]
  cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  qemuargs                 = [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", "${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores}"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=rt"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "win10.iso"
  output_directory         = "output/bootstrap"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "45m"
  vm_name                  = "windows10-bootstrap-x86_64"
}

source "qemu" "debuggee" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  cd_files                 = ["debuggee.ps1"]
  cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  qemuargs                 = [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", "${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores}"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=rt"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "output/bootstrap/windows10-bootstrap-x86_64"
  disk_image               = true
  output_directory         = "output/debuggee"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "20m"
  vm_name                  = "windows10-debuggee-x86_64"
}

source "qemu" "debugger" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  cd_files                 = ["debugger.ps1", "debuggee.debugtarget"]
  cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  qemuargs                 = [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", "${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores}"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=rt"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "output/bootstrap/windows10-bootstrap-x86_64"
  disk_image               = true
  output_directory         = "output/debugger"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "20m"
  vm_name                  = "windows10-debugger-x86_64"
}

build {
  sources = ["source.qemu.bootstrap", "source.qemu.debuggee", "source.qemu.debugger"]

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File F:/stage1.ps1"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File F:/stage2.ps1"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/debuggee.ps1"]
    only   = ["qemu.debuggee"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/debugger.ps1"]
    only   = ["qemu.debugger"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/bootstrap/windows10-bootstrap-x86_64.run.sh <<EOF
#!/usr/bin/env bash
/usr/bin/qemu-system-x86_64 \\
  -name windows10-bootstrap-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows10-bootstrap-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -bios /usr/share/ovmf/x64/OVMF.fd -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=rt
EOF
chmod +x output/bootstrap/windows10-bootstrap-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.bootstrap"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/debuggee/windows10-debuggee-x86_64.run.sh <<EOF
#!/usr/bin/env bash
/usr/bin/qemu-system-x86_64 \\
  -name windows10-debuggee-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows10-debuggee-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -bios /usr/share/ovmf/x64/OVMF.fd -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -chardev socket,path=/tmp/krnldbg0,server=on,wait=off,id=char0 -serial chardev:char0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=rt
EOF
chmod +x output/debuggee/windows10-debuggee-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.debuggee"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/debugger/windows10-debugger-x86_64.run.sh <<EOF
#!/usr/bin/env bash
/usr/bin/qemu-system-x86_64 \\
  -name windows10-debugger-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows10-debugger-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -bios /usr/share/ovmf/x64/OVMF.fd -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -chardev socket,path=/tmp/krnldbg0,id=char0 -serial chardev:char0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=rt
EOF
chmod +x output/debugger/windows10-debugger-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.debugger"]
  }
}
