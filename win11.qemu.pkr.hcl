variable "cpu_cores" {
  type = number
  default = 4
}

variable "memory" {
  type = number
  default = 4096
}

variable "headless" {
  type = bool
  default = false
}

source "qemu" "bootstrap" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  shutdown_timeout         = "45m"
  boot_command             = [""]
  boot_wait                = "1s"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  #cd_files                 = ["drivers/*", "Autounattend.xml", "configure.ps1", "drivers.ps1", "winrm.ps1", "stage1.ps1", "stage2.ps1"]
  #cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=host"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "install.iso"
  output_directory         = "output/bootstrap"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "45m"
  vm_name                  = "windows11-bootstrap-x86_64"
}

source "qemu" "debuggee" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  shutdown_timeout         = "45m"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  cd_files                 = ["debuggee.ps1"]
  cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=host"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "output/bootstrap/windows11-bootstrap-x86_64"
  disk_image               = true
  output_directory         = "output/debuggee"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "20m"
  vm_name                  = "windows11-debuggee-x86_64"
}

source "qemu" "debugger" {
  shutdown_command         = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  shutdown_timeout         = "45m"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  skip_compaction          = true
  disk_compression         = false
  cd_files                 = ["debugger.ps1", "debuggee.debugtarget"]
  cd_label                 = "CD"
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-rtc", "base=utc,clock=host"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "output/bootstrap/windows11-bootstrap-x86_64"
  disk_image               = true
  output_directory         = "output/debugger"
  communicator             = "winrm"
  winrm_username           = "user"
  winrm_password           = "resu"
  winrm_insecure           = true
  winrm_timeout            = "20m"
  vm_name                  = "windows11-debugger-x86_64"
}

build {
  sources = ["source.qemu.bootstrap", "source.qemu.debuggee", "source.qemu.debugger"]

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/stage1.ps1"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/stage2.ps1"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/debuggee.ps1"]
    only   = ["qemu.debuggee"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy Bypass -File E:/debugger.ps1"]
    only   = ["qemu.debugger"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/bootstrap/windows11-bootstrap-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name windows11-bootstrap-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows11-bootstrap-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -cpu Skylake-Client-v4 -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/bootstrap/windows11-bootstrap-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.bootstrap"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/debuggee/windows11-debuggee-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name windows11-debuggee-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows11-debuggee-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -cpu Skylake-Client-v4 -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -chardev socket,path=/tmp/krnldbg0,server=on,wait=off,id=char0 -serial chardev:char0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/debuggee/windows11-debuggee-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.debuggee"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/debugger/windows11-debugger-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name windows11-debugger-x86_64 \\
  -machine type=q35,accel=kvm \\
  -device virtio-vga-gl -display gtk,gl=on \\
  -drive file=windows11-debugger-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -cpu Skylake-Client-v4 -smp ${var.cpu_cores},sockets=1,dies=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user0 -device virtio-net,netdev=user0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -chardev socket,path=/tmp/krnldbg0,id=char0 -serial chardev:char0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/debugger/windows11-debugger-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.debugger"]
  }
}
