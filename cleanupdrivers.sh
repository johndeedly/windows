#!/usr/bin/env bash
# remove all drivers not amd64 win10
find drivers/ -type f \( -not -iwholename '*/amd64/w10/*' \) \
    \( -not -iwholename '*/w10/amd64/*' \) \
    \( -not -iwholename '*.md' \) \
    \( -not -iwholename '*/cert/*' \) \
    \( -not -iwholename '*/guest-agent/*' \) \
    \( -not -name '.*' \) \
    -delete
# remove empty folders
find drivers/ -type d -delete
