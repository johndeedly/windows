#!/usr/bin/env bash

if [ ! -d win11-unpack ]; then
  7z x win11.iso -owin11-unpack
fi

cp -r drivers win11-unpack/
cp Autounattend.vm.xml win11-unpack/Autounattend.xml
cp configure.ps1 drivers.ps1 winrm.ps1 stage1.ps1 stage2.ps1 debuggee.ps1 debugger.ps1 win11-unpack/

if [ ! -d vbox-guest ]; then
  7z x /usr/lib/virtualbox/additions/VBoxGuestAdditions.iso -ovbox-guest
fi
mkdir -p win11-unpack/drivers/guest-agent
cp vbox-guest/VBoxWindowsAdditions-*.exe win11-unpack/drivers/guest-agent/
cp -r vbox-guest/cert win11-unpack/drivers/

find win11-unpack/ -type d -print -exec chmod 0700 {} \;
find win11-unpack/ -type f -print -exec chmod 0644 {} \;

mkisofs -b boot/etfsboot.com -no-emul-boot -c BOOT.CAT -iso-level 4 -J -l -D -N -joliet-long -relaxed-filenames -v -V "CCCOMA_X64FRE_DE-DE_DV9" -udf -boot-info-table -eltorito-alt-boot -eltorito-boot efi/microsoft/boot/efisys_noprompt.bin -no-emul-boot -o install.iso win11-unpack
