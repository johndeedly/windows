bcdedit /debug ON
bcdedit /set debugtype serial
bcdedit /set debugport 1
bcdedit /set baudrate 115200

# $otherboot = bcdedit /copy '{current}' /d 'Windows 10 Debugging' | Select-String -Pattern '{[-0-9A-F]+?}' | Select-Object -ExpandProperty Matches | Select-Object -ExpandProperty Value
# Write-Host "New boot entry guid is $($otherboot)"
# bcdedit /debug "$($otherboot)" ON
# bcdedit /set "$($otherboot)" debugtype serial
# bcdedit /set "$($otherboot)" debugport 1
# bcdedit /set "$($otherboot)" baudrate 115200
# #bcdedit /set "$($otherboot)" debug on
# #bcdedit /set "$($otherboot)" debugtype net
# #bcdedit /set "$($otherboot)" port 50000
# #bcdedit /set "$($otherboot)" hostip 169.254.0.1
# #bcdedit /set "$($otherboot)" dhcp no
# #bcdedit /set "$($otherboot)" key a.b.c.d
# #bcdedit /set "$($otherboot)" busparams 0.8.0
# bcdedit /default "$($otherboot)"
