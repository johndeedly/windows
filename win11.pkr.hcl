source "virtualbox-iso" "bootstrap" {
  headless             = false
  boot_command         = ["<enter>"]
  boot_wait            = "1s"
  guest_os_type        = "Windows11_64"
  disk_size            = 524288
  format               = "ovf"
  hard_drive_interface = "sata"
  iso_checksum         = "none"
  iso_interface        = "sata"
  iso_url              = "win11.iso"
  shutdown_command     = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  guest_additions_mode = "disable"
  output_directory     = "output/bootstrap"
  output_filename      = "../windows11-bootstrap-x86_64"
  cd_files             = ["Autounattend.xml", "configure.ps1", "drivers.ps1", "winrm.ps1", "stage1.ps1", "stage2.ps1"]
  cd_label             = "CD"
  communicator         = "winrm"
  winrm_username       = "user"
  winrm_password       = "resu"
  winrm_insecure       = true
  vboxmanage           = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "4096", "--audio-driver", "pulse", "--audioout", "on", "--usb", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vboxsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "off", "--accelerate2dvideo", "off", "--vram", "256", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"], ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "20", "--device", "0", "--type", "dvddrive", "--medium", "/usr/lib/virtualbox/additions/VBoxGuestAdditions.iso"]]
  vboxmanage_post      = [["modifyvm", "{{ .Name }}", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"], ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "20", "--device", "0", "--type", "dvddrive", "--medium", "none"]]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-ovf" "debuggee" {
  headless             = false
  format               = "ovf"
  shutdown_command     = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  guest_additions_mode = "disable"
  output_directory     = "output/debuggee"
  output_filename      = "../windows11-debuggee-x86_64"
  source_path          = "output/windows11-bootstrap-x86_64.ovf"
  cd_files             = ["debuggee.ps1"]
  cd_label             = "CD"
  communicator         = "winrm"
  winrm_username       = "user"
  winrm_password       = "resu"
  winrm_insecure       = true
  #vboxmanage           = [["modifyvm", "{{ .Name }}", "--nic2", "intnet", "--cableconnected2", "on", "--intnet2", "localdebugging"]]
  vboxmanage           = [["modifyvm", "{{ .Name }}", "--uart1", "0x3F8", "4", "--uartmode1", "server", "/tmp/krnldbg0"]]
  vboxmanage_post      = [["modifyvm", "{{ .Name }}", "--cableconnected1", "off"]]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-ovf" "debugger" {
  headless             = false
  format               = "ovf"
  shutdown_command     = "shutdown /s /t 10 /f /d p:4:1 /c Packer_Provisioning_Shutdown"
  guest_additions_mode = "disable"
  output_directory     = "output/debugger"
  output_filename      = "../windows11-debugger-x86_64"
  source_path          = "output/windows11-bootstrap-x86_64.ovf"
  cd_files             = ["debugger.ps1", "debuggee.debugtarget"]
  cd_label             = "CD"
  communicator         = "winrm"
  winrm_username       = "user"
  winrm_password       = "resu"
  winrm_insecure       = true
  #vboxmanage           = [["modifyvm", "{{ .Name }}", "--nic2", "intnet", "--cableconnected2", "on", "--intnet2", "localdebugging"],["sharedfolder", "add", "{{ .Name }}", "--name", "symbols", "--hostpath", "symbols", "--automount"]]
  vboxmanage           = [["sharedfolder", "add", "{{ .Name }}", "--name", "symbols", "--hostpath", "symbols", "--automount"]]
  vboxmanage_post      = [["modifyvm", "{{ .Name }}", "--uart1", "0x3F8", "4", "--uartmode1", "client", "/tmp/krnldbg0"],["sharedfolder", "remove", "{{ .Name }}", "--name", "symbols"]]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

build {
  sources = ["source.virtualbox-iso.bootstrap", "source.virtualbox-ovf.debuggee", "source.virtualbox-ovf.debugger"]

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy RemoteSigned -File A:/stage1.ps1"]
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "windows-restart" {
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy RemoteSigned -File A:/stage2.ps1"]
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy RemoteSigned -File A:/debuggee.ps1"]
    only   = ["virtualbox-ovf.debuggee"]
  }

  provisioner "windows-shell" {
    inline = ["powershell -NoLogo -ExecutionPolicy RemoteSigned -File A:/debugger.ps1"]
    only   = ["virtualbox-ovf.debugger"]
  }
}
