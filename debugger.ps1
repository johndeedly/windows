New-Item -Path "$ENV:USERPROFILE" -Name "build" -ItemType "directory" -Force | Out-Null

# WinDbg Preview
Write-Host "[#] trying to find the downloadlink for windbg preview"
$WebResponse = Invoke-WebRequest -Method 'POST' -Uri 'https://store.rg-adguard.net/api/GetFiles' -Body "type=PackageFamilyName&url=Microsoft.WinDbg_8wekyb3d8bbwe&ring=Retail" -ContentType 'application/x-www-form-urlencoded' -UseBasicParsing -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"
$LinkMatch = $WebResponse.Links | where { $_ -like '*_neutral*.appx*' } | Select-String -Pattern '(?<=a href=").+(?=" r)' | Select-Object -ExpandProperty Matches | Select-Object -ExpandProperty Value -Last 1
Write-Host "[#] downloading from $($LinkMatch)"
Invoke-WebRequest -Uri $LinkMatch -OutFile "$ENV:USERPROFILE/build/windbg.appx" -UseBasicParsing -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"
Write-Host "[#] installing $ENV:USERPROFILE/build/windbg.appx"
Start-Process -NoNewWindow -Wait -FilePath "$ENV:WINDIR/System32/dism.exe" -WorkingDirectory "$ENV:WINDIR/System32" -ArgumentList "/Online","/Add-ProvisionedAppxPackage","/PackagePath:`"$ENV:USERPROFILE/build/windbg.appx`"","/SkipLicense"
Copy-Item -Path "D:/debuggee.debugtarget" -Destination "$ENV:USERPROFILE/Desktop" -PassThru
Write-Host "[#] cleaning up build dir"
Remove-Item -Path "$ENV:USERPROFILE/build/windbg.appx" -Force -ErrorAction SilentlyContinue

# install Rizin
Write-Host "[#] trying to find the downloadlink for rizin"
$RizinUrl = ((Invoke-RestMethod -uri 'https://api.github.com/repos/rizinorg/rizin/releases/latest').assets | Where { $_.browser_download_url.EndsWith('-x86_64.exe') } | Select -First 1).browser_download_url
Write-Host "[#] downloading from $($RizinUrl)"
Invoke-WebRequest $RizinUrl -OutFile "$ENV:USERPROFILE/build/rizin_installer-latest-x86_64.exe" -UseBasicParsing -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"
Write-Host "[#] start installing rizin"
Start-Process -NoNewWindow -Wait -FilePath "$ENV:USERPROFILE/build/rizin_installer-latest-x86_64.exe" -WorkingDirectory "$ENV:USERPROFILE/build" -ArgumentList "/SILENT","/ALLUSERS","/NORESTART"
Write-Host "[#] cleaning up build dir"
Remove-Item -Path "$ENV:USERPROFILE/build/rizin_installer-latest-x86_64.exe" -Force -ErrorAction SilentlyContinue

# Symbols for Windows 10 (this'll take a freaking long time)
#Write-Host "Downloading debug symbols"
#C:/Windows/System32/cmd.exe /c setx /M "_NT_SYMBOL_PATH" "srv*C:\ProgramData\Dbg\sym*https://msdl.microsoft.com/download/symbols"
#New-Item -Path "C:/ProgramData" -Name "Dbg" -ItemType "directory" -Force | Out-Null
#New-Item -Path "C:/ProgramData/Dbg" -Name "sym" -ItemType "directory" -Force | Out-Null
#if (Test-Path -Path "Z:/" -PathType Container) {
#  Copy-Item -Path "Z:/*" -Destination "C:/ProgramData/Dbg/sym" -PassThru -Recurse
#}
#$syspaths = "$ENV:WINDIR","$ENV:WINDIR/System32","$ENV:WINDIR/System32/Drivers","$ENV:WINDIR/SysWOW64"
#$files = Get-ChildItem $syspaths | Where { $_.Extension -in ".exe",".dll",".sys" } | Select-Object -ExpandProperty FullName
#$filecnt = $files.Count
#$jobs = @()
#$idx = 0
#$filelock = [System.Threading.ReaderWriterLockSlim]::new()
#foreach ($file in $files) {
#  $idx += 1
#  $jobs += Start-ThreadJob -ScriptBlock {
#    $jobfile = $using:file
#    $jobidx = $using:idx
#    $jobcnt = $using:filecnt
#    $lock = $using:filelock
#    $pdb = & "C:/Program Files/Rizin/bin/rz-bin.exe" -I $jobfile | Select-String -Pattern 'dbg_file +([^$]+)' | Select-Object -ExpandProperty Matches | Select-Object -ExpandProperty Groups | Select-Object -ExpandProperty Value -Skip 1
#    $guid = & "C:/Program Files/Rizin/bin/rz-bin.exe" -I $jobfile | Select-String -Pattern 'guid +([0-9a-fA-F]+)' | Select-Object -ExpandProperty Matches | Select-Object -ExpandProperty Groups | Select-Object -ExpandProperty Value -Skip 1
#    if ($pdb -And $guid) {
#      $pdb = Split-Path $pdb -Leaf
#      $linktodbg = "http://msdl.microsoft.com/download/symbols/$($pdb)/$($guid)/$($pdb)"
#      $locallink = "C:/ProgramData/Dbg/sym/$($pdb)/$($guid)/$($pdb)"
#      if (-Not(Test-Path -Path $locallink -PathType Leaf)) {
#        $StatusCode = ""
#        $downloadlink = ""
#        try {
#          $Response = Invoke-WebRequest -Uri $linktodbg -Method "Head" -UseBasicParsing -UserAgent "Microsoft-Symbol-Server/10.0.10240.9" -MaximumRedirection 0 -ErrorAction SilentlyContinue
#          $StatusCode = $Response.StatusCode
#          $downloadlink = $Response.Headers.Location
#        } catch {
#          $StatusCode = $_.Exception.Response.StatusCode.value__
#        }
#        # microsoft redirects from symbol to the real download server, so 302 means success here
#        if ($StatusCode -eq 302) {
#          New-Item -Path "C:/ProgramData/Dbg/sym" -Name $pdb -ItemType "directory" -Force | Out-Null
#          New-Item -Path "C:/ProgramData/Dbg/sym/$($pdb)" -Name $guid -ItemType "directory" -Force | Out-Null
#          # ask again because of multithreading and filename changes
#          if (-Not(Test-Path -Path $locallink -PathType Leaf)) {
#            Invoke-WebRequest -Uri $downloadlink -OutFile $locallink -UseBasicParsing -UserAgent "Microsoft-Symbol-Server/10.0.10240.9"
#            try {
#              $lock.EnterWriteLock()
#              if (Test-Path -Path "Z:/" -PathType Container) {
#                New-Item -Path "Z:/" -Name $pdb -ItemType "directory" -Force | Out-Null
#                New-Item -Path "Z:/$($pdb)" -Name $guid -ItemType "directory" -Force | Out-Null
#                Copy-Item -Path $locallink -Destination "Z:/$($pdb)/$($guid)/$($pdb)" -PassThru -Recurse
#              }
#              Write-Host "[$($jobidx)/$($jobcnt)] $($jobfile),$($pdb),$($guid),$($downloadlink)"
#              Add-Content "$ENV:USERPROFILE/build/symbols_success.csv" "$($jobfile),$($pdb),$($guid),$($downloadlink)"
#            } finally {
#              $lock.ExitWriteLock()
#            }
#          }
#        } else {
#          try {
#            $lock.EnterWriteLock()
#            Write-Host "[$($jobidx)/$($jobcnt)] $($jobfile),#$($StatusCode)"
#            Add-Content "$ENV:USERPROFILE/build/symbols_failure.csv" "$($jobfile),#$($StatusCode)"
#          } finally {
#            $lock.ExitWriteLock()
#          }
#        }
#      } else {
#        try {
#          $lock.EnterWriteLock()
#          Add-Content "$ENV:USERPROFILE/build/symbols_success.csv" "$($jobfile),$($pdb),$($guid)"
#        } finally {
#          $lock.ExitWriteLock()
#        }
#      }
#    } else {
#      try {
#        $lock.EnterWriteLock()
#        Write-Host "[$($jobidx)/$($jobcnt)] $($jobfile),#nopdborguid"
#        Add-Content "$ENV:USERPROFILE/build/symbols_failure.csv" "$($jobfile),#nopdborguid"
#      } finally {
#        $lock.ExitWriteLock()
#      }
#    }
#  } -StreamingHost $Host
#}
#
#Write-Host "[#] processing..."
#Wait-Job -Job $jobs | Out-Null

#$adapter = Get-NetAdapter -Name "Ethernet 2"
#If (($adapter | Get-NetIPConfiguration).IPv4Address.IPAddress) {
# $adapter | Remove-NetIPAddress -AddressFamily "IPv4" -Confirm:$false
#}
#If (($adapter | Get-NetIPConfiguration).Ipv4DefaultGateway) {
# $adapter | Remove-NetRoute -AddressFamily "IPv4" -Confirm:$false
#}
#$adapter | New-NetIPAddress -AddressFamily "IPv4" -IPAddress "169.254.0.1" -PrefixLength 24
