New-Item -Path "$ENV:USERPROFILE" -Name "build" -ItemType "directory" -Force | Out-Null
Start-Transcript -path "$ENV:USERPROFILE/build/stage2.txt" -append

Write-Output "Enable Global Confirmation in Chocolatey"
choco feature enable -n allowGlobalConfirmation

Write-Output "Installing Microsoft Visual C++ Runtime"
choco install vcredist-all
Write-Output "Installing Notepad++"
choco install notepadplusplus
Write-Output "Installing 7zip"
choco install 7zip
Write-Output "Installing Mozilla Firefox"
choco install firefox
Write-Output "Installing Chromium"
choco install chromium
Write-Output "Installing Git"
choco install git
Write-Output "Installing TortoiseGit"
choco install tortoisegit

Write-Output "Installing .NET SDK 6"
choco install dotnet-6.0-sdk
Write-Output "Installing .NET SDK 7"
choco install dotnet-7.0-sdk
Write-Output "Installing .NET Runtime 6"
choco install dotnet-6.0-runtime
Write-Output "Installing .NET Runtime 7"
choco install dotnet-7.0-runtime
Write-Output "Installing ASP.NET Runtime 6"
choco install dotnet-6.0-aspnetruntime
Write-Output "Installing ASP.NET Runtime 7"
choco install dotnet-7.0-aspnetruntime
Write-Output "Installing .NET Desktop Runtime 6"
choco install dotnet-6.0-desktopruntime
Write-Output "Installing .NET Desktop Runtime 7"
choco install dotnet-7.0-desktopruntime
Write-Output "Installing Python 3.11"
choco install python311
Write-Output "Installing Visual Studio Code"
choco install vscode

# ===
# WSL (stage 2)
# ===
# Write-Output "Enable WSL (stage 2)"
# Write-Output "[#] WSL Install"
# wsl --install

# ===
# Exit
# ===

Stop-Transcript
exit 0
