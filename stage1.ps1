New-Item -Path "$ENV:USERPROFILE" -Name "build" -ItemType "directory" -Force | Out-Null
Start-Transcript -path "$ENV:USERPROFILE/build/stage1.txt" -append

# ===
# Powershell
# ===

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module -Name ThreadJob -MinimumVersion 2.0.3 -Force

# ===
# WinGet
#   Bug https://github.com/microsoft/winget-cli/issues/256
# ===

# Write-Output "Downloading WinGet and dependencies..."
# Write-Output "[#] vc libs"
# Invoke-WebRequest "https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx" -OutFile "$ENV:USERPROFILE/build/Microsoft.VCLibs.x64.14.Desktop.zip" -UseBasicParsing
# Expand-Archive -Path "$ENV:USERPROFILE/build/Microsoft.VCLibs.x64.14.Desktop.zip" -DestinationPath "$ENV:USERPROFILE/build/WinGet" -Force
# 
# Write-Output "[#] xaml lib"
# Invoke-WebRequest "https://www.nuget.org/api/v2/package/Microsoft.UI.Xaml/2.7.3" -OutFile "$ENV:USERPROFILE/build/Microsoft.UI.Xaml.zip" -UseBasicParsing
# Expand-Archive -Path "$ENV:USERPROFILE/build/Microsoft.UI.Xaml.zip" -DestinationPath "$ENV:USERPROFILE/build/Microsoft.UI.Xaml"
# Rename-Item -Path "$ENV:USERPROFILE/build/Microsoft.UI.Xaml/tools/AppX/x64/Release/Microsoft.UI.Xaml.2.7.appx" -NewName "Microsoft.UI.Xaml.2.7.zip"
# Expand-Archive -Path "$ENV:USERPROFILE/build/Microsoft.UI.Xaml/tools/AppX/x64/Release/Microsoft.UI.Xaml.2.7.zip" -DestinationPath "$ENV:USERPROFILE/build/WinGet" -Force
# 
# Write-Output "[#] winget"
# $WinGetUrl = ((Invoke-RestMethod -uri 'https://api.github.com/repos/microsoft/winget-cli/releases/latest').assets | Where { $_.browser_download_url.EndsWith('msixbundle') } | Select -First 1).browser_download_url
# Invoke-WebRequest $WinGetUrl -OutFile "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller.zip" -UseBasicParsing
# Expand-Archive -Path "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller.zip" -DestinationPath "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller"
# Rename-Item -Path "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller/AppInstaller_x64.msix" -NewName "AppInstaller_x64.zip"
# Expand-Archive -Path "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller/AppInstaller_x64.zip" -DestinationPath "$ENV:USERPROFILE/build/WinGet" -Force
# 
# Write-Output "[#] cleaning up build dir"
# Remove-Item -Path "$ENV:USERPROFILE/build/Microsoft.VCLibs.x64.14.Desktop.zip" -Force -ErrorAction SilentlyContinue
# Remove-Item -Path "$ENV:USERPROFILE/build/Microsoft.UI.Xaml.zip" -Force -ErrorAction SilentlyContinue
# Remove-Item -Path "$ENV:USERPROFILE/build/Microsoft.UI.Xaml" -Recurse -Force -ErrorAction SilentlyContinue
# Remove-Item -Path "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller.zip" -Force -ErrorAction SilentlyContinue
# Remove-Item -Path "$ENV:USERPROFILE/build/Microsoft.DesktopAppInstaller" -Recurse -Force -ErrorAction SilentlyContinue
# 
# Write-Output "[#] moving winget to program files"
# Move-Item -Path "$ENV:USERPROFILE/build/WinGet" -Destination "$ENV:PROGRAMFILES" -PassThru
# cmd /c setx /M PATH "%PATH%;$ENV:PROGRAMFILES\WinGet"

# ===
# Chocolatey
# ===

Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# ===
# WSL (stage 1)
# ===
# Write-Output "Enable WSL (stage 1)"
# Write-Output "[#] Microsoft-Windows-Subsystem-Linux"
# Start-Process dism.exe -ArgumentList "/online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart" -Wait -NoNewWindow
# Write-Output "[#] VirtualMachinePlatform"
# Start-Process dism.exe -ArgumentList "/online /enable-feature /featurename:VirtualMachinePlatform /all /norestart" -Wait -NoNewWindow

# ===
# Exit
# ===

Stop-Transcript
exit 0
