Write-Output "Installing Notepad++"
winget install Notepad++.Notepad++ --accept-package-agreements --accept-source-agreements
Write-Output "Installing 7zip"
winget install 7zip.7zip --accept-package-agreements --accept-source-agreements
Write-Output "Installing Mozilla Firefox"
winget install Mozilla.Firefox --accept-package-agreements --accept-source-agreements
Write-Output "Installing Chromium"
winget install Hibbiki.Chromium --accept-package-agreements --accept-source-agreements
Write-Output "Installing Git"
winget install Git.Git --accept-package-agreements --accept-source-agreements
Write-Output "Installing TortoiseGit"
winget install TortoiseGit.TortoiseGit --accept-package-agreements --accept-source-agreements

Write-Output "Installing .NET SDK 6"
winget install Microsoft.DotNet.SDK.6 --accept-package-agreements --accept-source-agreements
Write-Output "Installing .NET SDK 7"
winget install Microsoft.DotNet.SDK.7 --accept-package-agreements --accept-source-agreements
Write-Output "Installing .NET Runtime 6"
winget install Microsoft.DotNet.Runtime.6 --accept-package-agreements --accept-source-agreements
Write-Output "Installing .NET Runtime 7"
winget install Microsoft.DotNet.Runtime.7 --accept-package-agreements --accept-source-agreements
Write-Output "Installing ASP.NET Runtime 6"
winget install Microsoft.DotNet.AspNetCore.6 --accept-package-agreements --accept-source-agreements
Write-Output "Installing ASP.NET Runtime 7"
winget install Microsoft.DotNet.AspNetCore.7 --accept-package-agreements --accept-source-agreements
Write-Output "Installing .NET Desktop Runtime 6"
winget install Microsoft.DotNet.DesktopRuntime.6 --accept-package-agreements --accept-source-agreements
Write-Output "Installing .NET Desktop Runtime 7"
winget install Microsoft.DotNet.DesktopRuntime.7 --accept-package-agreements --accept-source-agreements
Write-Output "Installing Python 3.11"
winget install Python.Python.3.11 --scope=machine --accept-package-agreements --accept-source-agreements
Write-Output "Installing Visual Studio Code"
winget install Microsoft.VisualStudioCode --accept-package-agreements --accept-source-agreements
